package com.diplom.service;

import com.diplom.domain.Claim;
import com.diplom.domain.Status;
import com.diplom.domain.dto.ClaimDto;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * ClaimService - сервис для работы с Заявками на 3д печать.
 * Является прослойкой для бизнес логики.
 */
public interface ClaimService {
    /**
     * Метод для получения всех Заявок на 3д печать.
     *
     * @return
     * @throws SQLException
     */
    List<Claim> all() throws SQLException;

    /**
     * Метод для добавления Заявки на 3д печать.
     *
     * @param claim - ДТО объект для сохранения
     * @return - количество добавленных записей
     */
    void add(ClaimDto claim);

    /**
     * Метод для удаления Заявки на 3д печать.
     *
     * @param id  - уникальный идентификатор Заявки
     */
    void delete(UUID id);

    /**
     * Метод для обновления Заявки на 3д печать.
     *
     * @param claim - ДТО объект для обновления
     * @param id - уникальный идентификатор Заявки
     * @return - количество обновленных записей
     */
    void put(ClaimDto claim, UUID id);

    /**
     * Метод для изменения статуса Заявки на 3д печать.
     *
     * @param claimDto - объект для передачи данных
     * @param status - статус Заявки
     */
    void changeStatus(ClaimDto claimDto, Status status);

    /**
     * Метод для отправки email письма на почту пользователя.
     */
    void sendEmail(String email, String fio);
}
