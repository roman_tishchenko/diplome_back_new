package com.diplom.service.impl;

import com.diplom.domain.Claim;
import com.diplom.domain.Claims;
import com.diplom.domain.MailSender;
import com.diplom.domain.Status;
import com.diplom.domain.dto.ClaimDto;
import com.diplom.domain.impl.MailClaim;
import com.diplom.domain.impl.PgClaim;
import com.diplom.domain.impl.PgClaims;
import com.diplom.service.ClaimService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@Service
public class ClaimServiceImpl  implements ClaimService {

    private final JdbcTemplate jdbcTemplate;
    private final MailSender mailSender;

    public ClaimServiceImpl(JdbcTemplate jdbcTemplate, MailSender mailSender) {
        this.jdbcTemplate = jdbcTemplate;
        this.mailSender = mailSender;
    }

    @Override
    public List<Claim> all() throws SQLException {
        Claims claims = new PgClaims(jdbcTemplate);
        return claims.all();
    }

    @Override
    public void add(ClaimDto claim) {
        Claims claims = new PgClaims(jdbcTemplate);
        claims.add(claim);
    }

    @Override
    public void delete(UUID id) {
        Claims claims = new PgClaims(jdbcTemplate);
        claims.delete(id);
    }

    @Override
    public void put(ClaimDto claim, UUID id) {
        Claims claims = new PgClaims(jdbcTemplate);
        claims.put(claim, id);
    }

    @Override
    public void changeStatus(ClaimDto claimDto, Status status) {
        Claim claim = new PgClaim(claimDto);
        claim.changeStatus(status);
    }

    @Override
    public void sendEmail(String email, String fio) {
        Claim claim = new MailClaim(email, fio, mailSender);
        claim.sendEmail();
    }

}
