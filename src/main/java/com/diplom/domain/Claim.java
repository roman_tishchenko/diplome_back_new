package com.diplom.domain;

/**
 * Claim - интерфейс для Заявки на 3д печать.
 */
public interface Claim {
    /**
     * Метод для изменения статуса Заявки на 3д печать.
     *
     * @param status - статус Заявки
     */
    default void changeStatus(Status status) {
        throw new RuntimeException("Not supported");
    };

    /**
     * Метод для отправки email письма на почту пользователя.
     */
    default void sendEmail() {
        throw new RuntimeException("Not supported");
    };
}
