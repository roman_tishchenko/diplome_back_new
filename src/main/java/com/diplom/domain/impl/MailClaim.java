package com.diplom.domain.impl;

import com.diplom.domain.Claim;
import com.diplom.domain.MailSender;


public final class MailClaim implements Claim {

    private String email;
    private String fio;
    private MailSender mailSender;

    public MailClaim(String email, String fio, MailSender mailSender) {
        this.email = email;
        this.fio = fio;
        this.mailSender = mailSender;
    }

    @Override
    public void sendEmail() {
        String message = String.format(
                "Добрый день, %s! " +
                        "Ваша заявка СОЗДАНА. Все подробности можете узнать в личном кабинете.", this.fio
        );
        mailSender.send(this.email,"Update Status your claim in A3D", message);
    }
}
