package com.diplom.domain.impl;


import com.diplom.domain.Claim;
import com.diplom.domain.Status;
import com.diplom.domain.dto.ClaimDto;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.UUID;

public final class PgClaim implements Claim {
    private JdbcTemplate jdbcTemplate;
    private UUID id;
    private Status status = Status.CREATED;
    private String fio;
    private String op;
    private int course;
    private String projectName;
    private String scientificDirector;
    private String contactNumber;
    private String contactEmail;
    private String description;
    private String checker;

    public PgClaim(UUID id, String fio, String op, Status status, int course, String project_name, String contact_email, String contact_number, String description, String scientific_director, String checker) {
        this.id = id;
        this.status = status;
        this.fio = fio;
        this.op = op;
        this.course = course;
        this.projectName = project_name;
        this.scientificDirector = scientific_director;
        this.contactNumber = contact_number;
        this.contactEmail = contact_email;
        this.description = description;
        this.checker = checker;
    }

    public PgClaim(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public PgClaim(ClaimDto claimDto) {
        this.id = claimDto.getId();
        this.fio = claimDto.getFio();
        this.op = claimDto.getOp();
        this.course = claimDto.getCourse();
        this.projectName = claimDto.getProjectName();
        this.scientificDirector = claimDto.getScientificDirector();
        this.contactNumber = claimDto.getContactNumber();
        this.contactEmail = claimDto.getContactEmail();
        this.description = claimDto.getDescription();
        this.checker = claimDto.getChecker();
    }

    @Override
    public void changeStatus(Status status) {
        this.jdbcTemplate.update("update claim set status = ? where id = ?", status.name(), this.id);
    }
}
