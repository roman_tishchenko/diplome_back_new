package com.diplom.domain.impl;


import com.diplom.domain.Claim;
import com.diplom.domain.Claims;
import com.diplom.domain.Status;
import com.diplom.domain.dto.ClaimDto;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public final class PgClaims implements Claims {

    private JdbcTemplate jdbcTemplate;

    public PgClaims(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    RowMapper<Claim> ROW_MAPPER = (ResultSet resultSet, int rowNum) -> new PgClaim(
            UUID.fromString(resultSet.getString("id")), resultSet.getString("fio"), resultSet.getString("op"), Status.valueOf(resultSet.getString("status")), resultSet.getInt("course"),
            resultSet.getString("project_name"), resultSet.getString("contact_email"), resultSet.getString("contact_number"),
            resultSet.getString("description"), resultSet.getString("scientific_director"), resultSet.getString("checker"));


    @Override
    public List<Claim> all() throws SQLException {
        return jdbcTemplate.query("select * from claim", ROW_MAPPER);
    }

    @Override
    public void add(ClaimDto claimDto) {
        jdbcTemplate.update("INSERT INTO claim (id, fio, checker, contact_email, contact_number, course," +
                        " description, op, project_name, scientific_director, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                UUID.randomUUID(),
                claimDto.getFio(), claimDto.getChecker(),
                claimDto.getContactEmail(),
                claimDto.getContactNumber(),
                claimDto.getCourse(),
                claimDto.getDescription(),
                claimDto.getOp(),
                claimDto.getProjectName(),
                claimDto.getScientificDirector(),
                claimDto.getStatus().name());
    }

    @Override
    public void delete(UUID id) {
        jdbcTemplate.update("delete from claim where id = ?", id);
    }

    @Override
    public void put(ClaimDto claim, UUID id) {
        jdbcTemplate.update("update claim set fio = ?,  checker = ? , contact_email = ?, contact_number = ?, course = ?, description = ?, op = ?," +
                " project_name = ?, scientific_director = ?, status= ? where id = ?", claim.getFio(), claim.getChecker(),
                claim.getContactEmail(),
                claim.getContactNumber(),
                claim.getCourse(),
                claim.getDescription(),
                claim.getOp(),
                claim.getProjectName(),
                claim.getScientificDirector(),
                claim.getStatus().name(),
                id);
    }
}
