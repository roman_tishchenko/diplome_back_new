package com.diplom.domain;

public enum Status {
    CREATED, IN_WORK, COMPLETED, CANCELED
}
