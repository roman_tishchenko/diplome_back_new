package com.diplom.domain;

import com.diplom.domain.dto.ClaimDto;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Claims - интерфейс для списка Заявок на 3д печать.
 */
public interface Claims {
    /**
     * Метод для получения всех Заявок на 3д печать.
     *
     * @return
     * @throws SQLException
     */
    List<Claim> all() throws SQLException;

    /**
     * Метод для добавления Заявки на 3д печать.
     *
     * @param claim - ДТО объект для сохранения
     * @return - количество добавленных записей
     */
    void add(ClaimDto claim);

    /**
     * Метод для удаления Заявки на 3д печать.
     *
     * @param id  - уникальный идентификатор Заявки
     */
    void delete(UUID id);

    /**
     * Метод для обновления Заявки на 3д печать.
     *
     * @param claim - ДТО объект для обновления
     * @param id - уникальный идентификатор Заявки
     * @return - количество обновленных записей
     */
    void put(ClaimDto claim, UUID id);
}
