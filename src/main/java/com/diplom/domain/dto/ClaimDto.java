package com.diplom.domain.dto;

import com.diplom.domain.Status;
import lombok.Data;

import java.util.UUID;

//Data Transfer Object
@Data
public class ClaimDto {

    private UUID id;

    private Status status = Status.CREATED;

    private String fio;

    private String op;

    private int course;

    private String projectName;

    private String scientificDirector;

    private String contactNumber;

    private String contactEmail;

    private String description;

    private String checker;
}
