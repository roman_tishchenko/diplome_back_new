package com.diplom.controller;

import com.diplom.domain.Claim;
import com.diplom.domain.Status;
import com.diplom.domain.dto.ClaimDto;
import com.diplom.service.impl.ClaimServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("claim")
@CrossOrigin
public class ClaimController {

    private final ClaimServiceImpl claimService;

    public ClaimController(ClaimServiceImpl claimService) {
        this.claimService = claimService;
    }

    @GetMapping
    public List<Claim> getAll() throws SQLException {
        return claimService.all();
    }

    @PostMapping
    public void add(@RequestBody ClaimDto claim) {
        claimService.add(claim);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") String id) {
        claimService.delete(UUID.fromString(id));
    }

    @PutMapping("{id}")
    public void update(@PathVariable("id") String id,
                       @RequestBody ClaimDto claim) {
        claimService.put(claim, UUID.fromString(id));
    }

    @GetMapping("/email")
    public void sendEmail(@RequestParam String email, @RequestParam String fio) {
        claimService.sendEmail(email, fio);
    }

    @PostMapping("/changeStatus")
    public void changeStatus(@RequestParam ClaimDto claimDto, @RequestParam Status status) {
        claimService.changeStatus(claimDto, status);
    }

}
