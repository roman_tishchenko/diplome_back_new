create table claim
(
    id                  uuid not null
        constraint claim_pkey
            primary key,
    checker             varchar(255),
    contact_email       varchar(255),
    contact_number      varchar(255),
    course              integer,
    description         varchar(255),
    fio                 varchar(255),
    op                  varchar(255),
    project_name        varchar(255),
    scientific_director varchar(255),
    status              varchar(255)
);

alter table claim
    owner to postgres;

