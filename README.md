# Dimplome_new

## Environment

### Windows
Установите переменные среды пользователя JAVA_HOME и PATH, чтобы они указывали на каталоги Java 8+ и Maven.
Например:
JAVA_HOME = C:\Program Files\Java\jdk-14
PATH = C:\Program Files\Java\jdk-14\bin;C:\apache-maven-3.6.3\bin

Для установки PostgreSQL перейдите на сайт https://www.postgresql.org и скачайте последнюю версию дистрибутива для Windows,
на сегодняшний день это версия PostgreSQL 11 (в 11 версии PostgreSQL поддерживаются только 64-х битные редакции Windows).
После загрузки запустите инсталлятор

## Compilation
mvn clean package

## Run app
mvn spring-boot:run

 